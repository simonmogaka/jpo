<?php

namespace JPO\Main\Controllers;

use JPO\Common\Controllers\BaseController as Controller;

class SponsorController extends Controller
{

	public function indexAction()
	{
		
	}

	public function addAction()
	{
		
		$sponsor = \JPO\Main\Models\Sponsor::find(' id > 2');
		
		$this->view->content = $this->_view
							    ->setParamToView('sponsor',  $sponsor )
			                    ->getPartial('sponsor/form');


		$sponsor_obj  = new \JPO\Main\Models\Sponsor();

		$sponsor_obj->save(['Kenya'], ['title']);



	}

	public function editAction()
	{
		
	}

	public function deleteAction()
	{
		
	}


}