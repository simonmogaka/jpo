<?php
namespace JPO\Common\Controllers;

use Phalcon\Mvc\Controller;


class BaseController extends Controller
{
	private $javascript = array();

	public function addJavaScript($script)	
    {
    	array_push($this->javascript, $script);
    	$this->view->javascript = $this->javascript;

    }

    public function getJavaScript()
    {
    	return $this->javascript;
    }	

	public function initialize()
    {
    	
		$this->_view = new \Phalcon\Mvc\View();
		$this->_view->setViewsDir('../apps/main/views/');	
		
		
		//$this->view->user = Profile::
        $this->view->javascript = $this->javascript;

		$main_navigation = $this->view->getPartial('sections/main_navigation');
		$search_form     = $this->view->getPartial('sections/search_form');
		
		$this->view->nav_bar_top_left = $this->view
								->setParamToView('main_navigation',  $main_navigation)
								->setParamToView('search_form',  $search_form)			                    
								->getPartial('sections/nav_bar_top_left'); 

		$user_account_menu = $this->view->getPartial('sections/user_account_menu');

		$this->view->nav_bar_top_right = $this->view
								->setParamToView('user_account_menu',  $user_account_menu)			                    								
								->getPartial('sections/nav_bar_top_right');

		$default_skin = 'skin-blue-light';

		$this->view->default_skin = $default_skin ;
    }
}